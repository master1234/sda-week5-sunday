package com.tallinn.arraylist;

import java.util.ArrayList;
import java.util.List;

/**
 * Add animals
 * dog, cat, tiger, bear, monkey
 * print
 *
 * Does donkey exist = print error.
 * print
 *
 * add the eagle on the 4th index.
 * print
 *
 * create a new list
 * put all the items of the animal list to this new list.
 *
 */
public class CreateArrayListExample {
    public static void main(String[] args) {
        // Creating an ArrayList of String
        List<String> animals = new ArrayList<>();
        // Adding new elements to the ArrayList
        animals.add("dog");
        animals.add("cat");
        animals.add("tiger");
        animals.add("bear");
        animals.add("monkey");
        System.out.println(animals);

        if(!animals.contains("donkey")){
            System.out.println("Donkey does not exist");
        }

        animals.add(4, "eagle");
        System.out.println(animals);

        List<String> newAnimalList = new ArrayList<>();
        newAnimalList.addAll(animals);
        System.out.println(newAnimalList);

        animals.clear();
        System.out.println(newAnimalList);


        List<String> cloneAnimalList = (List)
                ((ArrayList<String>)newAnimalList).clone();
        System.out.println(newAnimalList);

    }
}
