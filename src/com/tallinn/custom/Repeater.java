package com.tallinn.custom;

import com.sun.deploy.util.StringUtils;

import java.util.Scanner;

public class Repeater {

    /**
     * Objective: We want to print numbers with the amount of its value.
     * Lets suppose we enter 5: we will print:
     * 55555
     * 5
     * 5
     * 5
     * 5
     * 5
     * Conditions:
     * We accept integer which is below 5
     * In case of 0 give information that we got nothing to print
     * In case of negative number or any data which is not integer: print error information .
     */
    public static void main(String[] args) {
        while(true){
            Repeater repeater = new Repeater();
            System.out.println("Welcome please enter the number");
            Scanner scanner = new Scanner(System.in);
            String incomingString = scanner.nextLine();

            if(incomingString.equalsIgnoreCase("exit")){
                System.out.println("Exiting");
                return;
            }

            boolean isInteger = repeater.isInteger(incomingString);

            if (isInteger) {
                int number = Integer.parseInt(incomingString);
                boolean isValid = repeater.isValidNumber(number);
                if (isValid) {
                    repeater.print(number);
                }

            } else {
                System.out.println("We only expect integer");
            }
        }



    }

    private void print(int number) {
        String result = "";
        //55555
        for (int counter = 0; counter < number; counter++) {
            result = result + number;
        }
        System.out.println(result);

        for (int counter = 0; counter < number; counter++) {
            System.out.println(number);
        }


    }

    private boolean isValidNumber(int number) {
        if (number > 5) {
            System.out.println("We only expect integer 1-5");
            return false;
        } else if (number == 0) {
            System.out.println("Nothing to print");
            return false;
        } else if (number < 0) {
            System.out.println("We dont expect negative values");
            return false;
        }
        return true;
    }

    boolean isInteger(String incomingString) {
        try {
            Integer.parseInt(incomingString);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

}
