package com.tallinn.homeworks;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RepeatedWords {

    public static void main(String args[])
    {

        RepeatedWords repeatedWords = new RepeatedWords();
        ArrayList<String> listRepeated = new ArrayList<>();
        System.out.println("Welcome please enter the string");
        Scanner scanner = new Scanner(System.in);

        String incomingString = scanner.nextLine();
        String[] words = incomingString.split(" ");
        ArrayList<String> listOfStrings = new ArrayList<>();

        for(int counter=0;counter<words.length;counter++){
            boolean isExists = repeatedWords.doesListContainsCaseInsensitive
                    (words[counter],listOfStrings);

            if(isExists){
                boolean isAdded= repeatedWords.
                        doesListContainsCaseInsensitive(words[counter]
                                ,listRepeated);
                if(!isAdded){
                    listRepeated.add(words[counter]);
                }

            }else{
                listOfStrings.add(words[counter]);
            }

        }

        System.out.println(listRepeated);

    }


    private boolean doesListContainsCaseInsensitive
            (String word, ArrayList<String> listOfStrings) {

        for(String element:listOfStrings){
            if(word.equalsIgnoreCase(element)){
                return true;
            }
        }
        return false;
    }



}
