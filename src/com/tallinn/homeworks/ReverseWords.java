package com.tallinn.homeworks;

import java.util.Scanner;

public class ReverseWords {

    public static void main(String[] args) {
        ReverseWords reverseWords = new ReverseWords();

        System.out.println("Welcome please enter the string");
        Scanner scanner = new Scanner(System.in);
        //scanner.next will take the first string
        //scanner.nextLine will take the sentence.
        String incomingString = scanner.nextLine();

        String[] words = incomingString.split(" ");

        /**
         * This is one way
         */
        String resultWithoutStringBuilder="";
        for(int counter=0;counter<words.length;counter++){
            String reverse = reverseWords.reverse(words[counter]);
            resultWithoutStringBuilder = resultWithoutStringBuilder
                    + reverse+ " ";

        }
        System.out.println(resultWithoutStringBuilder);

        String resultWithStringBuilder = "";
        for (int counter = 0; counter < words.length; counter++) {
            String reverse = new StringBuilder(words[counter])
                    .reverse().toString();
            resultWithStringBuilder = resultWithStringBuilder
                    + reverse + " ";
        }
        System.out.println(resultWithStringBuilder);


    }



    private String reverse (String incomingString){
        if(incomingString==null || incomingString.isEmpty()){
            return incomingString;
        }
        String reverse = "";
        for (int i = incomingString.length() - 1; i >= 0; i--) {
            reverse = reverse + incomingString.charAt(i);
        }
        return reverse;
    }

}
