package com.tallinn.homeworks;

import java.util.Scanner;

public class PalindromeWords {

    public static void main(String[] args) {
        PalindromeWords palindromeWords = new PalindromeWords();

        System.out.println("Welcome please enter the string");
        Scanner scanner = new Scanner(System.in);
        String incomingString = scanner.nextLine();
        String[] words = incomingString.split(" ");

        //Anna jumped to second level. I said to her "WOW".

        String resultWithStringBuilder = "";
        for (int counter = 0; counter < words.length; counter++) {
            String filteredWord = palindromeWords.eraseNonLetters(words[counter]);
            String reverse = new StringBuilder(filteredWord).reverse().toString();
            if(reverse.length()>=2 && reverse.equalsIgnoreCase(filteredWord)){
                resultWithStringBuilder = resultWithStringBuilder+filteredWord+" ";
            }
        }
        System.out.println(resultWithStringBuilder);


    }

    private String eraseNonLetters(String word) {
        String result = word.replaceAll("/[^A-Za-z0-9]/","");
        result = result.replaceAll("\\p{Punct}", "");
        return result;
    }


}
